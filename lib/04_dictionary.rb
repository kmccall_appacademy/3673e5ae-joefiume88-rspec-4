class Dictionary
  # TODO: your code goes here!
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(new_entries)
    if new_entries.is_a?(String)
      @entries[new_entries] = nil
    elsif new_entries.is_a?(Hash)
      @entries.merge!(new_entries)
    end
  end

  def keywords
    @entries.keys.sort { |x, y| x <=> y }
  end

  def include?(keyword)
    @entries.has_key?(keyword)
  end

  def find(snippet)
    @entries.select do |word, definition|
      word.match(snippet)
    end
  end

  def printable
    entries = keywords.map do |keyword|
      %Q{[#{keyword}] "#{@entries[keyword]}"}
    end

    entries.join("\n")
  end
end
